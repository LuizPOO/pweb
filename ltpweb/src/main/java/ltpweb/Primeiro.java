package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/primeiro")
public class Primeiro extends HttpServlet {
	
	private int contadordeAcerto = 0;
	private int contadordeErro = 0;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado.");
		System.out.println("Contador de acertos inicial: " + contadordeAcerto);
		System.out.println("Contador de erros inicial: " + contadordeErro);
		
	}

	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String usuario = req.getParameter ("nome");
		String senha = req.getParameter ("senha");
	
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Acesso de login via HTTP POST</h1>");
		out.println("<h3>Seja bem vindo! " + "</h3>");
		out.println("<h3>Usu�rio: " + req.getParameter("nome") + "</h3>");
		out.println("<h3>Senha: " + req.getParameter("senha") + "</h3>");
		
		if(usuario.equals("admin") && senha.equals("admin")){
			
			HttpSession session = req.getSession();
			
			contadordeAcerto = contadordeAcerto + 1;
		
		out.println("<h3>Id da sess�o: " + session.getId () + "</h3>");
		out.println("<h3>Tentativas CERTAS: " + contadordeAcerto + "</h3>");
		out.println("<h3>Tentativas ERRADAS: " + contadordeErro + "</h3>");
		out.println("<a href=\"fim\"> sair </a>");
		out.println("</body>");
		out.println("</html>");
		
		}
		
		else {
			
			contadordeErro = contadordeErro + 1;
	    		
	       	out.println("<html>");
	   		out.println("<head>");
	   		out.println("</head>");
	   		out.println("<body>");
	   		out.println("<h2>Usu�rio ou Senha invalido(a)!</h2>");
	   		out.println("<h3>Tentativas CERTAS: " + contadordeAcerto + "</h3>");
			out.println("<h3>Tentativas ERRADAS: " + contadordeErro + "</h3>");
			out.println("</body>");
	   		out.println("</html>");
	   		
		}
   		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String usuario = req.getParameter ("nome");
		String senha = req.getParameter ("senha");
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Acesso de login via HTTP GET</h1>");
		out.println("<h3>Seja bem vindo! " + "</h3>");
		out.println("<h3>Usu�rio: "+ req.getParameter("nome") + "</h3>");
		out.println("<h3>Senha: "+ req.getParameter("senha") + "</h3>");
		
		if(usuario.equals("admin") && senha.equals( "admin")){
			
			HttpSession session = req.getSession();
			
			contadordeAcerto = contadordeAcerto + 1;
		
		out.println("<h3>Id da sess�o: " + session.getId () + "</h3>");
		out.println("<h3>Tentativas CERTAS: " + contadordeAcerto + "</h3>");
		out.println("<h3>Tentativas ERRADAS: " + contadordeErro + "</h3>");
		out.println("</body>");
		out.println("</html>");
		
		}
		
		else {
			
			contadordeErro = contadordeErro + 1;
	    		
	       	out.println("<html>");
	   		out.println("<head>");
	   		out.println("</head>");
	   		out.println("<body>");
	   		out.println("<h2>Usuario ou Senha invalido(a)!</h2>");
	   		out.println("<h3>Tentativas CERTAS: " + contadordeAcerto + "</h3>");
			out.println("<h3>Tentativas ERRADAS: " + contadordeErro + "</h3>");
			out.println("</body>");
	   		out.println("</html>");
	   		
		}
		
	}
		
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
		System.out.println("Contador de acertos final: " + contadordeAcerto);
		System.out.println("Contador de erros final: " + contadordeErro);
		
		}

}
