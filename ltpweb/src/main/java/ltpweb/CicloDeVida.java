package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ciclovida")
public class CicloDeVida extends HttpServlet {
	
	private int contador;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado!");
		System.out.println("Contador inicial [ " + contador + " ]");
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Contagem de acesso [ " + ++contador + " ]" + "</h1>");
		out.println("</body>");
		out.println("</html>");
	}

	@Override
	public void destroy() {
		System.out.println("Servlet Destru�do!");
		System.out.println("Contador final [ " + contador + " ]");
	}
}
